from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class ArenaHome:

    def __init__(self, browser: Chrome):
        self.browser = browser

    def verify_arena_home_is_open(self):
        # Czekanie na stronę
        wait = WebDriverWait(self.browser, 10)
        tick_box = (By.ID, "onlyActiveRelease")
        wait.until(expected_conditions.presence_of_element_located(tick_box))

    def open_admin_panel(self):
        administrative_settings_button = self.browser.find_element_by_css_selector(".header_admin a")
        administrative_settings_button.click()
