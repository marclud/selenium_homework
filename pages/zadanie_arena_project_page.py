from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class ProjectPage:

    def __init__(self, browser: Chrome):
        self.browser = browser

    def verify_project_page_is_open(self, project_name):
        # Czekanie na stronę
        wait = WebDriverWait(self.browser, 10)
        title = (By.CLASS_NAME, "content_label_title")
        wait.until(expected_conditions.text_to_be_present_in_element(title, project_name))

    def go_to_projects_list(self):
        project_button = self.browser.find_element_by_css_selector("[href = 'http://demo.testarena.pl/administration/projects']")
        project_button.click()
