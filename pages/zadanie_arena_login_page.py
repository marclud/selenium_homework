from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class ArenaLoginPage:

    def __init__(self, browser: Chrome):
        self.browser = browser

    def verify_login_page_is_open(self):
        # Czekanie na stronę
        wait = WebDriverWait(self.browser, 10)
        email_field = (By.ID, "email")
        wait.until(expected_conditions.presence_of_element_located(email_field))

    def login(self, admistrator_email, administrator_password):
        # Zainicjowanie elementów (login, password, login button)
        login = self.browser.find_element_by_id("email")
        password = self.browser.find_element_by_id("password")
        login_button = self.browser.find_element_by_id("login")

        # Logowanie
        login.send_keys(admistrator_email)
        password.send_keys(administrator_password)
        login_button.click()
