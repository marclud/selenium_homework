from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class ArenaAdminProjectsPanel:

    def __init__(self, browser: Chrome):
        self.browser = browser

    def verify_arena_admin_panel_is_open(self):
        # Czekanie na stronę
        wait = WebDriverWait(self.browser, 10)
        add_project_button = (By.CSS_SELECTOR, "[href='http://demo.testarena.pl/administration/add_project']")
        wait.until(expected_conditions.presence_of_element_located(add_project_button))

    def go_to_add_new_project_page(self):
        add_project_button = self.browser.find_elements_by_class_name("button_link")
        add_project_button[0].click()

    def find_project_name(self, project_name):
        wait = WebDriverWait(self.browser, 10)
        projects_list = (By.CSS_SELECTOR, "td > a")
        assert wait.until(expected_conditions.text_to_be_present_in_element(projects_list, project_name))
