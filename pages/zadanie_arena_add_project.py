import random
import string

from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class AddProjectPage:

    def __init__(self, browser: Chrome):
        self.browser = browser
        self.project_name = None
        self.prefix = None

    def verify_add_project_page_is_open(self):
        # Czekanie na stronę
        wait = WebDriverWait(self.browser, 10)
        name_field = (By.ID, "name")
        wait.until(expected_conditions.presence_of_element_located(name_field))

    def add_project_name(self, length):
        self.project_name = ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))
        project_name_field = self.browser.find_element_by_id("name")
        project_name_field.send_keys(self.project_name)

    def add_prefix(self, length):
        self.prefix = ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))
        prefix_field = self.browser.find_element_by_id("prefix")
        prefix_field.send_keys(self.prefix)

    def save_new_project(self):
        save_button = self.browser.find_element_by_css_selector("[name=save]")
        save_button.click()
