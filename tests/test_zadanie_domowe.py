import pytest
from selenium.webdriver import Chrome
from webdriver_manager.chrome import ChromeDriverManager

from pages.zadanie_arena_add_project import AddProjectPage
from pages.zadanie_arena_admin_projects_panel import ArenaAdminProjectsPanel
from pages.zadanie_arena_login_page import ArenaLoginPage
from pages.zadanie_arena_home import ArenaHome
from pages.zadanie_arena_project_page import ProjectPage


@pytest.fixture()
def browser():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony
    browser.get("http://demo.testarena.pl/zaloguj")

    # Zwrot danych do testu
    yield browser

    # Zamknięcie przeglądarki
    browser.quit()


def test_adding_new_project(browser):
    # Logowanie do TestArena
    login_page = ArenaLoginPage(browser)
    login_page.verify_login_page_is_open()
    login_page.login("administrator@testarena.pl", "sumXQQ72$L")  ## Login i hasło

    # Otwieranie admin panel
    arena_home = ArenaHome(browser)
    arena_home.verify_arena_home_is_open()
    arena_home.open_admin_panel()

    # Otwieranie strony dodawania nowego projektu
    arena_admin_panel = ArenaAdminProjectsPanel(browser)
    arena_admin_panel.verify_arena_admin_panel_is_open()
    arena_admin_panel.go_to_add_new_project_page()

    # Dodawanie nowego projektu
    add_project_page = AddProjectPage(browser)
    add_project_page.verify_add_project_page_is_open()
    add_project_page.add_project_name(10)
    add_project_page.add_prefix(5)
    add_project_page.save_new_project()

    # Otwieranie strony z listą projektów (Projects)
    project_page = ProjectPage(browser)
    project_page.verify_project_page_is_open(
        add_project_page.project_name)  # weryfikacja po nazwie dopiero co utworzonego projektu
    project_page.go_to_projects_list()

    # Wyszukiwanie nowo utworzonego projektu po nazwie
    arena_admin_panel.verify_arena_admin_panel_is_open()
    arena_admin_panel.find_project_name(add_project_page.project_name)
